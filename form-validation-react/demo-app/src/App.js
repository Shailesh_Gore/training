import logo from './logo.svg';
import './App.css';
import React from 'react';
//import { BrowserRouter,Route, Routes} from 'react-router-dom';
import {
  BrowserRouter,
  Switch,
  Route,
  Routes,
  Link
  } from 'react-router-dom';
//import Switch from 'react-router-dom'
import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Navbar from './components/Navbar';
import Error from './components/pages/Error';
import Post from './components/pages/Post';

function App() {
  return(

    <>
    
    <BrowserRouter>
    <Navbar/>
    
    <Routes>
    <Route  path="/" element={<Home/>}/>
    <Route  path="/about" element={<About/>}/>
    
   <Route path="/contact">
    <Contact company_name="geeky"/>
   </Route>

    <Route  path="/post/:category"  element ={<Post/>}/>
    </Routes>
    
    </BrowserRouter>
    
    
    </>

  ); 
    
   
}

export default App;
