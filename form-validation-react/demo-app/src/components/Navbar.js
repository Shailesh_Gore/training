import React from 'react'
import {Link,NavLink} from "react-router-dom"
const Navbar=()=>{
    return(
        <div>
        <nav>
            <ul>
                <li>
                   <NavLink to="/" activeStyle={{fontWeight:"bold",color:"red"}} exact>Home</NavLink>
                </li>

                <li>
                   <NavLink to="about" activeStyle={{fontWeight:"bold",color:"red"}} exact>About</NavLink>
                </li>

                <li>
                   <NavLink to="contact" activeStyle={{fontWeight:"bold",color:"red"}} exact>Contact</NavLink>
                </li>

                <li>
                   <NavLink to="post/mobile" activeStyle={{fontWeight:"bold",color:"red"}} exact>Post</NavLink>
                </li>
            </ul>
        </nav>
        </div>
    );
}
export default Navbar;