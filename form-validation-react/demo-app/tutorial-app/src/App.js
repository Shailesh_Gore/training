
import { createElement ,useState} from 'react';
import React from 'react'
import './App.css';
import Expenses from './components/Expenses/Expenses.js';
import NewExpense from './components/NewExpense/NewExpense';
import ChartBar from './components/Chart/ChartBar';
//import ExpenseItem from './components/ExpenseItem';
const DUMMY_EXPENSES = [
  {
    id: "e1",
    title: "Car Insurance",
    Amount: 262.23,
    date: new Date(2021, 5, 12)
  },
  {
    id: "e2",
    title: "TV",
    Amount: 288.23,
    date: new Date(2021, 7, 23)
  },
  {
    id: "e3",
    title: "Computer",
    Amount: 399.23,
    date: new Date(2021, 11, 19)
  }
];


function App() {
  
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler=(expense)=>{
   setExpenses((prevExpenses)=>{
     return [expense,...prevExpenses];
   });
  
  };

  return (
    // React.createElement('div',{},React.createElement('h2',{},"Let's get started"),
    // React.createElement(Expenses,{items:expenses}))
    <div>
      <NewExpense onAddExpense={addExpenseHandler}/>
      <Expenses items={expenses}></Expenses>
      
    </div>
  );
}

export default App;
