import React from 'react'
import ExpenseItem from './ExpenseItem';
import './ExpensesList.css'
const ExpensesList=(props)=>{
    //let expensesContent= <p style={{color:"red"}}>no expenses are found</p>

    if(props.items.length===0){
        return <h2 className="expenses-list__fallback">found no expenses.</h2>
    }
    return <ul className="expenses-list">

        {(props.items.map((expense) => (
        <ExpenseItem
        key={expense.id}
         title={expense.title}
          Amount={expense.Amount}
          date={expense.date} />
      )))}
    </ul>
};
export default ExpensesList;