import Card from '../UI/Card';
import ExpenseDate  from '../Expenses/ExpenseDate';

//import './ExpenseItem.css';
import '../Expenses/ExpenseItem.css';
import { useState } from 'react';

function ExpenseItem(props) {
    
    // const [title,setTitle] = useState(props.title);
    // const [amount,setAmount] = useState(props.amount);
    // console.log("React item just evaluated..");
    

    // const clickHandler =()=>{
    //     setTitle('updated!!!!')
        
    //     console.log(title);

    // };
    
    return (
        <li>
        <Card className="expense-item">
           <ExpenseDate date={props.date}></ExpenseDate>
            <div className="expense-item__description">
                <h2>{props.title}</h2>
                <div className="expense-item__price">${props.Amount}</div>
               { console.log("in the amount")}
            </div>
            {/* {<button onClick={clickHandler}>change item</button> } */}
        </Card></li>
    ); 
}

export default ExpenseItem;