import Card from "../UI/Card";
import ExpenseItem from "../Expenses/ExpenseItem";
import "./Expenses.css";
import ExpenseFilter from "./ExpenseFilter";
import { useState } from "react/cjs/react.development";
import ExpensesList from "./ExpensesList";
import ExpensesChart from "./ExpensesChart";
function Expenses(props) {

  const [filteredYear, setFilteredYear] = useState('2020');

  const filterChangeHandler = (selectedYear) => {
    // console.log('In Expense.js');
    // console.log(selectedYear);
    setFilteredYear(selectedYear);
  };


  
  const filteredExpenses=props.items.filter(expense => {
    return expense.date.getFullYear().toString() === filteredYear;
  })


  // let expensesContent= <p style={{color:"red"}}>no expenses are found</p>

  // if(filteredExpenses.length>0){
  //   expensesContent= (filteredExpenses.map((expense) => (
  //     <ExpenseItem
  //     key={expense.id}
  //      title={expense.title}
  //       Amount={expense.Amount}
  //       date={expense.date} />
  //   )))
  // }

  return (


    
      <div>
        <Card className="expenses">
        <ExpenseFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />

       <ExpensesChart expenses={filteredExpenses}/>
        {/* {expensesContent} */}
        <ExpensesList items={filteredExpenses}/>

        </Card>

      </div>
    

  );
}
export default Expenses;