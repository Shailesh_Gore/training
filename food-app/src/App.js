import { useState} from 'react';
import './App.css';
import Header from "../src/components/Layout/Header";
import { Fragment } from 'react';
import Meals from './components/Meals/Meals';
import Cart from './components/Cart/Cart';
import CartProvider from '../src/store/CartProvider';

function App() {

const [showCart,setShowCart] = useState(false);

const showCartHandler=() =>{
setShowCart(true);
} 

const closeCartHandler=()=>{
  setShowCart(false);
}

  return (
    <CartProvider>
     {showCart && <Cart onCloseCart={closeCartHandler}/>}
    <Header cartIsClicked={showCartHandler}/>

    <main>
      <Meals/>
    </main>
   
    </CartProvider>
  );
}

export default App;
