import React, { useReducer } from "react";
import CartContext from "./cart-context";

const initialState= {
    items:[],
    totalAmount:0
}

const reducer = (state,action)=>{

    if(action.type==='ADD'){

        const addTheItems = state.items.concat(state.item);
        const updateTheTotalAmount = state.totalAmount + action.item.price * action.item.amount;

        return {
            item: addTheItems,
            totalAmount:updateTheTotalAmount
        }
    }

    if(action.type==='DEC'){
        return state.totalAmount-1;
    }


    return
}



const CartProvider=(props)=>{

    

    const [stateCart,dispatch] = useReducer(reducer,initialState);


    const addItemToCart =(item)=>{
    return dispatch({type:'ADD' ,item:item})

    }
    const RemoveItemFromCart =(id)=>{

        return dispatch({type:'REMOVE',id:id})

    }
    

    

    const cartContext = {
        items:stateCart.items,
        totalAmount:stateCart.totalAmount,
        additionItem:addItemToCart,
        removeItem:RemoveItemFromCart
    }



    return <CartContext.Provider value={cartContext}>
        {props.children}
    </CartContext.Provider>
}
export default CartProvider;