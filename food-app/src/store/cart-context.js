import React from "react";
const  CartContext=React.createContext(
    {
        items:[],
        totalAmount:0,
        addition:(items)=>{},
        removeItem:(id)=>{}
    }
)
export default CartContext;