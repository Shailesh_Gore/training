import { useRef } from "react";
import Inputs from "../UI/Inputs";
import classes from "./MealitemsForm.module.css";
const MealitemForm =props=>{
    const amountInputRef = useRef();
    const [amountIsValid,setAmountIsValid] = useState(true);
    const submitHandler=(e)=>{
        e.preventDefault();

        const enteredAmount = amountInputRef.current.value;
        const enteredAmountNumber = +enteredAmount;

        if(enteredAmount.trim().length ===0||enteredAmountNumber<1||enteredAmountNumber>5){
            return setAmountIsValid(false);
        }

    }


    useRef(null);

        return (

            <form className={classes.form} onSubmit={submitHandler}>
                <Inputs label="Amount"  ref={amount} input={{
                        id:'amount',
                        type:'number',
                        
                        min:'1',
                        max:'5',
                        step:'1',
                        defaultValue:'1'
                } }            
                />
                <button className={classes.button}>+Add</button>
                <p>{!amountIsValid && `please Enter the Valid Amount`}</p>
            </form>

        );
}
export default MealitemForm;