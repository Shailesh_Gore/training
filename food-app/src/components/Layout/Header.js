
import { Fragment } from "react";
import images from "../../assets/meals.jpeg";
import classes from "./Header.module.css";
import HeaderCartButton from "./HeaderCartButton";
const Header = (props) => {

    return (
        <Fragment>
           <header className={classes.header}>
               <h1>Foodie</h1>
               <HeaderCartButton justClick={props.cartIsClicked}/>
               
           </header>
            <div className={classes['main-image']}>
                <img src={images} alt="the food is delicious" />
                
            </div>
            
        </Fragment>
    );

}
export default Header;