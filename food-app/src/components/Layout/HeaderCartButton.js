import react, {  useContext } from "react";
import CartContext from "../../store/cart-context";
import CartIcon from "../Cart/CartIcon";
import classes from "./HeaderCartButton.module.css";
const HeaderCartButton=props=>{

const Ctx = useContext(CartContext);
const numberOfItems = Ctx.items.reduce((curNumber,item)=>{return curNumber+item.amount},0);
   



return(
<button className={classes.button} onClick={props.justClick}>
<span className={classes.icon}>
<CartIcon/>
</span>
<span>
    Cart
</span>
<span className={classes.badge}>
{numberOfItems}
</span>

</button>
);
}
export default HeaderCartButton;