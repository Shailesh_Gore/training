
import Modal from "../UI/Modal";
import classes from "./Cart.module.css";
const Cart = props => {

    const cartItems = <ul className={classes['cart-items']}>  {[{ id: 'c1', name: 'sushi', amount: 2, price: 23.33 }].map((items) => ( <li>{items.name}</li> ))}</ul>

    return (
        //understanding the tutorial
        //trying to add new items
        <Modal onCloseCart={props.onCloseCart}>
            <div>
            {cartItems}
            </div>
            <div className={classes.total}>
                <span>Total</span>
                <span>34</span>
            </div>
            <div className={classes.actions}>
                <button className={classes['button--alt'] } onClick={props.onCloseCart}>cancel</button>
                <button className={classes.button}>order</button>
            </div>

        </Modal>

    );
}
export default Cart;