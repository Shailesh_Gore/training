import react from "react";


const Home=()=> {
    return (
     <>
    <div className="container-fluid nav-bg">
        <div className="row">
            <div className="col-10 mx-auto">
                <div className="col-md-6 pt-5 pt-lg-0 order-2 order-lg-1">
                    <h1>grow your business with <strong>Shailesh Gore</strong></h1>
                    <h2 className="my-3">We are the Team</h2>
                    <div className="mt-3">
                        <a className="btn-get-started">Get Started</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
     </>
    );
  }
  
  export default Home;
