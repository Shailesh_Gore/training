
import './App.css';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle";
import Home from './Home.js'
import About from './About.js'
import Contact from './Contact.js'
import Service from './Service.js'
import {Route, Switch,Redirect} from 'react-router-dom';
import NavBar from './NavBar';
function App() {
  return (
   <>
   <NavBar/>
   <Switch>
     <Route exact path='/' component={Home} />
     <Route exact path='/about' component={About} />
     <Route exact path='/contact' component={Contact} />
     <Route exact path='/service' component={Service} />
      <Redirect to="/"/>
   </Switch>
   </>
  );
}

export default App;
