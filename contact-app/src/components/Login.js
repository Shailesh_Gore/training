import React, { useState } from "react";
import { Button, ButtonGroup } from "semantic-ui-react";
import './Login.css';

 const Login =()=>{

    const [login,setLogin]=useState("");
    const [email,setEmail]=useState("");

    const [entry,setEntry]=useState([]);

    
   

    const loginFormSubmit=(e)=>{
        e.preventDefault();
        
        const newEntry={login:login, email:email};
        setEntry([...entry,newEntry]);
        console.log(entry);
        setLogin("");
        setEmail("");   
        
    }
    
    
     return (
         <>
         <div className="card">
         <form onSubmit={loginFormSubmit}>
             <h1>Login</h1>
             <input type="text" placeholder="user name"  value={login} onChange={(e)=>{
                 setLogin(e.target.value);
             }}/><br></br><br></br>
             <input type="password"  placeholder="password" value={email}  onChange={(e)=>{
                 setEmail(e.target.value);
             }}/><br></br><br></br>
             <button type="submit">Login</button>
         </form>
         </div><br></br><br></br>
         
        
            {
                entry.map((curElt)=>{
                   return(
                    <div className="loginCard">
                        
                    <span>{curElt.login}</span>
                    
                        <span style={{marginLeft:"5rem"}}>{curElt.email}</span>
                    </div>
                   ) 
                })
            }
        

         </>
     );

 }
 export default Login;