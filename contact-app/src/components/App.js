
import React, {useEffect, useState} from 'react';
import AddContact from './AddContact';
import {v4 as uuid } from 'uuid';
import './App.css';
import ContactList from './ContactList';
import Header from './Header';

function App() {
  const LOCAL_STORAGE_KEY = "contacts";
  const [contacts,setContacts]=useState([]);//currently im updating it
//git ///again
  useEffect(()=>{
    const retrievContacts = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if(retrievContacts)
    setContacts(retrievContacts);
  },[])

  useEffect(()=>{
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(contacts))
  },[contacts])
//ignore the git 
//ignore the status

  const addContactHandler=(contact)=>{
    console.log(contact);
    setContacts([...contacts, {  id: uuid(),...contact }]);
  };

  

  return (
    <div className='ui container'>
     <Header/>
     <br></br>
     <br></br>
     <AddContact addContactHandler={addContactHandler}/>
     <br></br>
     <ContactList contacts={contacts}/>
      
    </div>
  );
}

export default App;
