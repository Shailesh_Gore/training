import ContactCard from "./ContactCard";

function ContactList(props) {

    

    const contactList = props.contacts.map(
        (contact)=>{
            return(
               
               <ContactCard contact={contact} delContact={removeContactHandler}/>
              
        )}
    )
    return (
      <div className="ui celled contact">
       {contactList} 
      </div>
    );
  }
  
  export default ContactList;
  