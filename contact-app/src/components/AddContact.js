import React,{Component} from "react";
class AddContact extends React.Component {
    state={
       name:"",
       email:"",
   } ;
 

 add=(e)=>{
        e.preventDefault();
        if(this.state.name==="" || this.state.email===""){
            alert("all fields are mandetory");
        }
        this.props.addContactHandler(this.state); 
        this.setState({name:" ", email:" "});

    }

    

onNameChangeHandler=(e)=>{
        return e.target.value;
    }

  render(){return (
     <div> 
        
    <div className="ui main">        
        
        
        <h2>AddContact</h2>
        <form className="ui form" onSubmit={this.add}>
            <div className="field">
                <label>Name</label>
            <input type="text" name="name" placeholder="Name" value={this.state.name} onChange={(e)=>this.setState({name:e.target.value})}/>
            </div>
            <div className="field">
                <label>email</label>
                <input type="email" name="email" placeholder="xyz12@abc.com" value={this.state.email} onChange={(e)=>this.setState({email:e.target.value})
                }/>
            </div>

            <button className="ui button blue">Add Contact</button>
        </form>
      
        </div>
        </div>
  );}
}

export default AddContact;
