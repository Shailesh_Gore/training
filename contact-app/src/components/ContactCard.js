import React from "react";
import user from "../components/userimg.png";


const ContactCard=(props)=>{
    const {email,id,name} = props.contact
    return (
        
        <div className="ui segment">
         <img className="ui avatar image" src={user} alt="user"/>
        <h2 className="ui left floated header">{name}</h2>

        <i className="trash alternate outline icon" style={{color:"red", marginTop:"7px",float:"right" }}
        onClick={props.delContact(id)}
        ></i>
        <div className="ui clearing divider"></div>
        <p>{email}</p>
      </div>
    )
}
export default ContactCard;