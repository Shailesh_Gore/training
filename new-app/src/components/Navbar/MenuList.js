export const MenuList=[
    {
        title:'Home',
        url:'/'
    },
    {
        title:'key Feature',
        url:'/feature'
    },
    {
        title:'Pricing',
        url:'/pricing'
    },
    {
        title:'Testimonial',
        url:'/testimonial'
    },
    {
        title:'Try for free',
        url:'/demo'
    },
];