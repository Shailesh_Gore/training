import React,{useState} from "react";
import { NavLink } from "react-router-dom";
import { MenuList } from "./MenuList";
import { FaBars,FaTimes } from 'react-icons/fa';
import './Navbar.css';
const Navbar=()=>{

    const [clicked,setClicked]=useState(false);

    const menulist=MenuList.map(({url,title},index)=>{
        return(
            <li key={index}>
                <NavLink to={url} activeClassName='active'>{title}</NavLink>
            </li>
        );

    })

    const onClickHandler=()=>{
        setClicked(!clicked);
    }
    return(
    <nav>
        <div className="logo">
        VPN <font>Lab</font>
        </div>
        
        <div className="menu-icon" onClick={onClickHandler}>
            
           {clicked?<FaTimes className="user-sidebar-menu-icon-times moveleft"/>:<FaBars/>}
            
        </div>

        <ul className={clicked?"menu-list":"menu-list close"}>
        {menulist}
        </ul>
    </nav>
    );
}
export default Navbar;