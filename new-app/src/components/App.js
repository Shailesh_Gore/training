import { BrowserRouter as Router, Switch, Route, Routes} from 'react-router-dom';
import './App.css';
import Demo from './pages/Demo';
import Home from './pages/Home';
import Keyfeature from './pages/Keyfeature';
import Pricing from './pages/Pricing';
import Testimonial from './pages/Testimonial';
import Navbar from './Navbar/Navbar';

function App() {
  return (
    <div className='container'>
      <Router>
      <Navbar/>
            
        <Routes>
        
        <Route path='/' element={<Home/>} />
        <Route path='/feature' element={<Keyfeature/>} />
        <Route path='/pricing' element={<Pricing/>} />
        <Route path='/testimonial' element={<Testimonial/>} />
        <Route path='/demo' element={<Demo/>} />
        
        </Routes>
       

      </Router>
      
    </div>
  );
}

export default App;
